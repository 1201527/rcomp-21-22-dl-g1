RCOMP 2021-2022 Project repository template
===========================================
# 1. Team members (update this information please) #
  * 1201459 - Luís Costa
  * 1201527 - Guilherme Sousa
  * 1201529 - Miguel Marques
  * 1201545 - Francisco Oliveira  


# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)

