!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
ip dhcp excluded-address 172.17.253.33 255.255.255.224
!
ip dhcp pool B3-WIFI
 network 172.17.251.64 255.255.255.192
 default-router 172.17.251.65
 dns-server 172.17.252.194
 domain-name building-3.rcomp-21-22-DL-G1
ip dhcp pool B3-F1
 network 172.17.252.0 255.255.255.192
 default-router 172.17.252.1
 dns-server 172.17.252.194
 domain-name building-3.rcomp-21-22-DL-G1
ip dhcp pool B3-F0
 network 172.17.252.128 255.255.255.192
 default-router 172.17.252.129
 dns-server 172.17.252.194
 domain-name building-3.rcomp-21-22-DL-G1
ip dhcp pool B3-DMZ
 network 172.17.252.192 255.255.255.224
 default-router 172.17.252.193
 dns-server 172.17.252.194
 domain-name building-3.rcomp-21-22-DL-G1
ip dhcp pool B3-VOIP
 network 172.17.253.32 255.255.255.224
 option 150 ip 172.17.253.33
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017R6X8-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 ip address 172.17.248.3 255.255.255.192
!
interface FastEthernet1/0.422
 encapsulation dot1Q 422
 ip address 172.17.251.65 255.255.255.192
!
interface FastEthernet1/0.425
 encapsulation dot1Q 425
 ip address 172.17.252.1 255.255.255.192
!
interface FastEthernet1/0.427
 encapsulation dot1Q 427
 ip address 172.17.252.129 255.255.255.192
!
interface FastEthernet1/0.428
 encapsulation dot1Q 428
 ip address 172.17.252.193 255.255.255.224
!
interface FastEthernet1/0.431
 encapsulation dot1Q 431
 ip address 172.17.253.33 255.255.255.224
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 1
 log-adjacency-changes
 network 172.17.251.64 0.0.0.63 area 3
 network 172.17.252.0 0.0.0.63 area 3
 network 172.17.252.128 0.0.0.63 area 3
 network 172.17.253.32 0.0.0.31 area 3
!
router rip
!
ip nat inside source static tcp 172.17.252.197 80 172.17.248.3 80 
ip nat inside source static tcp 172.17.252.197 443 172.17.248.3 443 
ip nat inside source static tcp 172.17.252.194 53 172.17.248.3 53 
ip nat inside source static udp 172.17.252.194 53 172.17.248.3 53 
ip classless
!
ip flow-export version 9
!
!
access-list 100 permit ip 172.17.251.64 0.0.0.63 any
access-list 100 permit icmp any any echo
access-list 100 permit icmp any any echo-reply
access-list 100 permit ip any host 255.255.255.255
access-list 100 permit udp any host 172.17.251.65 eq tftp
access-list 100 permit tcp any host 172.18.83.65 eq 2000
access-list 100 permit ospf any host 224.0.0.5
access-list 100 deny ip any host 172.17.248.5
access-list 100 permit ip any any
access-list 101 permit ip 172.17.252.0 0.0.0.63 any
access-list 101 permit icmp any any echo
access-list 101 permit icmp any any echo-reply
access-list 101 permit ip any host 255.255.255.255
access-list 101 permit udp any host 172.17.252.1 eq tftp
access-list 101 permit tcp any host 172.17.252.1 eq 2000
access-list 101 permit ospf any host 224.0.0.5
access-list 101 deny ip any host 172.17.248.5
access-list 101 permit ip any any
access-list 102 permit ip 172.17.252.128 0.0.0.63 any
access-list 102 permit icmp any any echo
access-list 102 permit icmp any any echo-reply
access-list 102 permit ip any host 255.255.255.255
access-list 102 permit udp any host 172.17.252.129 eq tftp
access-list 102 permit tcp any host 172.17.252.129 eq 2000
access-list 102 permit ospf any host 224.0.0.5
access-list 102 deny ip any host 172.17.248.5
access-list 102 permit ip any any
access-list 103 permit ip 172.17.253.32 0.0.0.31 any
access-list 103 permit icmp any any echo
access-list 103 permit icmp any any echo-reply
access-list 103 permit ip any host 255.255.255.255
access-list 103 permit udp any host 172.17.253.33 eq tftp
access-list 103 permit tcp any host 172.17.253.33 eq 2000
access-list 103 permit ospf any host 224.0.0.5
access-list 103 deny ip any host 172.17.248.5
access-list 103 permit ip any any
access-list 105 deny ip 172.17.251.64 0.0.0.63 any
access-list 105 deny ip 172.17.252.0 0.0.0.63 any
access-list 105 deny ip 172.17.252.128 0.0.0.63 any
access-list 105 deny ip 172.17.252.192 0.0.0.31 any
access-list 105 deny ip 172.17.253.32 0.0.0.31 any
access-list 105 permit ip any host 255.255.255.255
access-list 105 permit icmp any any echo
access-list 105 permit icmp any any echo-reply
access-list 105 permit udp any host 172.17.248.5 eq tftp
access-list 105 permit tcp any host 172.17.248.5 eq 2000
access-list 105 permit tcp any host 172.17.248.5 eq 1720
access-list 105 permit ospf any host 224.0.0.5
access-list 105 permit tcp any host 172.17.248.5 eq www
access-list 105 permit tcp any host 172.17.248.5 eq 443
access-list 105 permit udp any host 172.17.248.5 eq domain
access-list 105 deny ip any host 172.17.248.5
access-list 105 permit ip any any
access-list 106 permit tcp any host 172.17.252.193 eq www
access-list 106 permit tcp any host 172.17.252.193 eq 443
access-list 106 permit udp any host 172.17.252.194 eq domain
access-list 106 permit ip 172.17.252.192 0.0.0.31 any
access-list 106 permit ip any host 255.255.255.255
access-list 106 permit udp any host 172.17.252.193 eq tftp
access-list 106 permit tcp any host 172.17.252.193 eq 2000
access-list 106 permit ospf any host 224.0.0.5
access-list 106 deny ip any host 172.17.248.5
access-list 106 permit ip any any
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 111
 session target ipv4:172.17.248.1
!
dial-peer voice 2 voip
 destination-pattern 222
 session target ipv4:172.17.248.2
!
dial-peer voice 4 voip
 destination-pattern 444
 session target ipv4:172.17.248.4
!
telephony-service
 max-ephones 25
 max-dn 25
 ip source-address 172.17.253.33 port 3000
 auto assign 1 to 2
!
ephone-dn 1
 number 3000
!
ephone-dn 2
 number 3001
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

