!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017N2F8-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 15.203.48.105 255.0.0.0
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 ip address 172.17.248.5 255.255.255.128
!
interface FastEthernet1/0.415
 encapsulation dot1Q 415
 no ip address
!
interface FastEthernet1/0.416
 encapsulation dot1Q 416
 ip address 172.17.248.129 255.255.255.128
!
interface FastEthernet1/0.417
 encapsulation dot1Q 417
 ip address 172.17.249.1 255.255.255.128
!
interface FastEthernet1/0.418
 encapsulation dot1Q 418
 ip address 172.17.249.129 255.255.255.128
!
interface FastEthernet1/0.419
 encapsulation dot1Q 419
 ip address 172.17.250.1 255.255.255.128
!
interface FastEthernet1/0.420
 encapsulation dot1Q 420
 ip address 172.17.250.129 255.255.255.128
!
interface FastEthernet1/0.421
 encapsulation dot1Q 421
 ip address 172.17.251.1 255.255.255.192
!
interface FastEthernet1/0.422
 encapsulation dot1Q 422
 ip address 172.17.251.65 255.255.255.192
!
interface FastEthernet1/0.423
 encapsulation dot1Q 423
 ip address 172.17.251.129 255.255.255.192
!
interface FastEthernet1/0.424
 encapsulation dot1Q 424
 ip address 172.17.251.193 255.255.255.192
!
interface FastEthernet1/0.425
 encapsulation dot1Q 425
 ip address 172.17.252.1 255.255.255.192
!
interface FastEthernet1/0.426
 encapsulation dot1Q 426
 ip address 172.17.252.65 255.255.255.192
!
interface FastEthernet1/0.427
 encapsulation dot1Q 427
 ip address 172.17.252.129 255.255.255.192
!
interface FastEthernet1/0.428
 encapsulation dot1Q 428
 ip address 172.17.252.193 255.255.255.224
!
interface FastEthernet1/0.429
 encapsulation dot1Q 429
 ip address 172.17.252.225 255.255.255.224
!
interface FastEthernet1/0.430
 encapsulation dot1Q 430
 ip address 172.17.253.1 255.255.255.224
!
interface FastEthernet1/0.431
 encapsulation dot1Q 431
 ip address 172.17.253.33 255.255.255.224
!
interface FastEthernet1/0.432
 encapsulation dot1Q 432
 ip address 172.17.253.65 255.255.255.240
!
interface FastEthernet1/0.433
 encapsulation dot1Q 433
 ip address 172.17.253.81 255.255.255.240
!
interface FastEthernet1/0.434
 encapsulation dot1Q 434
 ip address 172.17.253.97 255.255.255.240
!
interface FastEthernet1/0.435
 encapsulation dot1Q 435
 ip address 172.17.253.113 255.255.255.240
!
interface Vlan1
 no ip address
 shutdown
!
router rip
!
ip classless
ip route 15.203.48.104 255.255.255.252 15.203.48.106 
ip route 172.17.248.128 255.255.255.128 172.17.248.1 
ip route 172.17.249.0 255.255.255.128 172.17.248.2 
ip route 172.17.249.128 255.255.255.128 172.17.248.1 
ip route 172.17.250.0 255.255.255.128 172.17.248.1 
ip route 172.17.250.128 255.255.255.128 172.17.248.4 
ip route 172.17.251.0 255.255.255.192 172.17.248.1 
ip route 172.17.251.64 255.255.255.192 172.17.248.3 
ip route 172.17.251.128 255.255.255.192 172.17.248.4 
ip route 172.17.251.192 255.255.255.192 172.17.248.2 
ip route 172.17.252.0 255.255.255.192 172.17.248.3 
ip route 172.17.252.64 255.255.255.192 172.17.248.1 
ip route 172.17.252.128 255.255.255.192 172.17.248.3 
ip route 172.17.252.192 255.255.255.224 172.17.248.3 
ip route 172.17.252.224 255.255.255.224 172.17.248.4 
ip route 172.17.253.0 255.255.255.224 172.17.248.2 
ip route 172.17.253.32 255.255.255.224 172.17.248.3 
ip route 172.17.253.64 255.255.255.240 172.17.248.2 
ip route 172.17.253.80 255.255.255.240 172.17.248.2 
ip route 172.17.253.96 255.255.255.240 172.17.248.4 
ip route 172.17.253.112 255.255.255.240 172.17.248.4 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

