RCOMP 2021-2022 Project - Sprint 3 - Member 1201529 folder
===========================================
#### Building 2 ####
===========


**Planning**
---------------------------

> When planning the sprint, each building's router got it's own IP, building 2's IP is 172.17.248.2


>
>
> Before starting the project, every VLAN dedicated to the group was properly distributed, building 2's VLANs:
>    * VLAN *417*: B2-WIFI : 120 *nodes*;
>    * VLAN *424*: B2-F1 : 50 *nodes*;
>    * VLAN *430*: B2-F0 : 25 *nodes*;
>    * VLAN *432*: B2-DMZ : 12 *nodes*;
>    * VLAN *433*: B2-VOIP : 12 *nodes*;
>
> When subnetting we gave each VLAN an ip
> This can be seen in the image below
>
>
![Projeto](building2network.png)
>
> Even though these are the only VLANs associated with building 2, every VLAN is present in the simulation




>
**Project in Packet Tracer**
----------------------------
>
> Following the previous' sprint design (and improving upon it), this is building 2's layout in packet tracer
>
![Projeto](building2_pkt.png)
>
>
>Again, even though not part of the building, the backbone's, including the other building's IC is present
>
>The connection to the internet is represented by a cloud with the ip of 15.203.48.106/30


>
> All doors not connected to end devices are in trunk mode
>

> For each building, a set of requirement were made relating to the end nodes (objects )
>   * To the VLAN for the ground-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the first-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the wi-fi it was connected an access point per floor, with a laptop per access point (it was only necessary 1 in total)
>   * To the VLAN for the servers it was connected a server per floor (it was only necessary 1 in total)
>   * To the VLAN for the phones it was connected a phone per floor (it was only necessary 1 in total)



## 1. OSPF dynamic routing
----------------------------
A primeira coisa foi a eliminação das routing tabbles
Como estabelicido no planning a área do edificio 2 seria 2 e a do backbone 0

O seguinte código foi usado:
-router ospf PROCESS-ID
-network NETWORK-ADDRESS NETWORK-WILDCARD area AREA-NUMBER


## 2. HTTP servers
----------------------------
-Foi adinicionado um novo servidor à local DMZ network que assegure os serviços HTTP e criou-se uma págima simples em HTML que identificasse o edíficio.
-Inicialmente foi adicionado o servidor que, posteriormente, foi connectado ao HC, de forma a que os serviços HTTP fossem assegurados.
-Como último passo, foi configurada uma simples página HTML, que poderá ser accesada pelos PC's presentes em cada edíficio.


## 3. DHCPv4 service
----------------------------
-Na descrição do projeto é nos pedido que configurássemos o router de forma a que este fornecesse o serviço DHCPv4 para todas as redes locais (dentro do prédio).
-Com o recurso às PL's fornecidas pelos docentes (PL05) os nomes das DHCP pool foram:
-B2-WIFI
-B2-F1
-B2-F0
-B2-DMZ
-Depois de configuradas todas as VLAN's, exceto a configuração do Voip que será feita segundo a opção 150 como nos é pedido no enunciado, todos os PC's passam a estar fornecidos com os serviços DHCP.


## 4 - VoIP service ##

Aos telefones 7960 colocados no packet tracer no sprint anterior foi estabelecida uma ligação atravéz do router.
Em cada building já se encontravam 2 telefones

Para cada switch ligado  a um telefone, foram configuradas as portas ligadas ao mesmo utilizado este comando na conexão ao telefone:

-int fa6/1
-    switchport mode access
-    switchport voice vlan 433 
-    no switchport access vlan



Posteriormente, no router foram criadas "DHCP pools":

-ip dhcp excluded-address 172.17.253.81 172.17.253.94
-   ip dhcp pool VOIP
-   option 150 ip 172.17.253.81
-   network 172.17.253.80 255.255.255.240


Depois, foram registados os telemóveis e os seus números. O número máximo de telemoveis foi retirado do sprint passado enquanto que os números de ligação foram escolhidos fazendo "numero de edificio x 1000"
O código é o seguinte:


-telephony-service
-   max-ephones 12
-   max-dn 12
-   ip source-address 172.17.253.81 port 2000
-   auto assign 1 to 2

-ephone-dn 1
-   number 2000 

-ephone-dn 2
-   number 2001



Com estes passos, já era possível fazer a ligação entre telefones do mesmo edificio, no entanto era ainda necessario estabelecer a conexão com outros edificios
Para realizar esta conexão foi feito este código para cada "voice VLAN" dos outros edificios

-dial-peer voice 426 voip
-   destination-pattern 1...
-   session target ipv4:172.17.248.1



Com isto, ficaram os telefones configurados e funcionais

## 5.DNS

 Foi criado, um subdominio com conhecimento ao dominio mais alto
 O DNS server (ns.buildin-2.rcomp-21-22-dl-g1) e o DNS domain name (buildin-2.rcomp-21-22-dl-g1)
 Foram adicionados os records e os servidores de todos os buildings manualmente como pedido no enunciado


## 6.NAT

 Foram efetuados os comandos no router do building 1 para as portas, 53(DNS), 443(HTTPS) e 80(HTTP).
 Para alem disso efetuei ainda os comandos -ip nat outside e -ip nat inside

O código é o seguinte:

- ip nat inside source static tcp 172.17.253.68 80 172.17.248.2 80
- ip nat inside source static tcp 172.17.253.68 443 172.17.248.2 443
- ip nat inside source static tcp 172.17.253.67 53 172.17.248.2 53
- ip nat inside source static udp 172.17.253.67 53 172.17.248.2 53


## 7. Static Firewall (ACLs)
----------------------------
- Inicialmente, foram bloqueadas as falsificações internas, exceto a DMZ.
- Depois, foram permitidos todos os pedidos ICMP e respostas echo
- O tráfego da DMZ para fora foi permitido. Todo o tráfego em direção à DMZ foi bloqueado exceto o tráfego DNS e HTTP/HTTPS.
- De seguida, foi bloqueado todo o tráfego direcionado a um router com IPV4 e pertencente ao mesmo.
- Finalmente, permitiu-se o tráfego que passa pelo router.

No total o código para todos os VLANs com a excessao do DMZ foi assim:

- access-list 100 permit ip 172.17.249.0 0.0.0.127 any
- access-list 100 permit icmp any any echo
- access-list 100 permit icmp any any echo-reply
- access-list 100 permit ip any host 255.255.255.255
- access-list 100 permit udp any host 172.17.249.1 eq tftp
- access-list 100 permit tcp any host 172.17.249.1 eq 2000
- access-list 100 permit ospf any host 224.0.0.5
- access-list 100 deny ip any host 172.17.248.5
- access-list 100 permit ip any any



Para o DMZ foi ligeiramente diferente:

- access-list 105 deny ip 172.17.249.0 0.0.0.127 any
- access-list 105 deny ip 172.17.251.192 0.0.0.63 any
- access-list 105 deny ip 172.17.253.0 0.0.0.31 any
- access-list 105 deny ip 172.17.253.64 0.0.0.15 any
- access-list 105 deny ip 172.17.253.80 0.0.0.15 any
- access-list 105 permit ip any host 255.255.255.255
- access-list 105 permit icmp any any echo
- access-list 105 permit icmp any any echo-reply
- access-list 105 permit udp any host 172.17.248.5 eq tftp
- access-list 105 permit tcp any host 172.17.248.5 eq 2000
- access-list 105 permit tcp any host 172.17.248.5 eq 1720
- access-list 105 permit ospf any host 224.0.0.5
- access-list 105 permit tcp any host 172.17.248.5 eq www
- access-list 105 permit tcp any host 172.17.248.5 eq 432
- access-list 105 permit udp any host 172.17.248.5 eq domain
- access-list 105 deny ip any host 172.17.248.5
- access-list 105 permit ip any any
- access-list 106 permit tcp any host 172.17.253.68 eq www
- access-list 106 permit tcp any host 172.17.253.68 eq 432
- access-list 106 permit udp any host 172.17.253.67 eq domain
- access-list 106 permit ip 172.17.253.64 0.0.0.31 any
- access-list 106 permit ip any host 255.255.255.255
- access-list 106 permit udp any host 172.17.253.65 eq tftp
- access-list 106 permit tcp any host 172.17.253.65 eq 2000
- access-list 106 permit ospf any host 224.0.0.5
- access-list 106 deny ip any host 172.17.248.5
- access-list 106 permit ip any any






#### Backbone ####
===========


No backbone apenas se juntou o trabalho realizado nos outros edificios.

Como juntamos o trabalho prosteriormente em vez de trabalhar já no backbone isto significou que certas partes (como os telefones) não ficaram funcionais no trabalho final ( no caso dos telefones devido a um bug do programa )