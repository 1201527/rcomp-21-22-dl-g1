RCOMP 2021-2022 Project - Sprint 3 planning
===========================================
Sprint master: 1201545


Planning Meeting
================

This document includes the sprint's backlog summary, general technical decisions
regarding the coordination of the requested tasks, and finally, the task
assigned to each team member.

 

### Sprint's Backlog Summary

In this sprint, we will keep working on the same network simulation from the
previous sprint (regarding the same building). From the already established
layer three configurations, now OSPF based dynamic routing will be used to
replace static routing used previously.

 

### General Technical Decisions

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*All member used Cisco Packet Tracer version 8.1.1.0022

* The main output is the Packet Tracer simulation file for the corresponding building, it should be named BuildingN.pkt, with N replaced by the letter identifying the building.

* The OSPF area ids:
    -Backbone: 0
    -Building 1: 1
    -Building 2: 2
    -Building 3: 3
    -Building 4: 4 



* The highest level DNS domain name for Building 1 is rcomp-21-22-dl-g1.


* For Building 2,3,4 and 5, the DNS domain name is building-X.rcomp-21-22-dl-g1, where X is the number that identifies each building.


* The DNS server name for Building 1 is ns.rcomp-21-22-dl-g1.

* For Building 2,3 and 4, the DNS server name is ns.building-X.rcomp-21-22-dl-g1, where X is the number that identifies each building.

* The IPv4 node address of the DNS name server:
    - ns.building-2.rcomp-21-22-dl-g1(Building 2)
        B2-DMZ: 172.17.253.66
    - ns.building-3.rcomp-21-22-dl-g1 (Building 3)
        B3-DMZ: 172.17.252.194
    - ns.building-4.rcomp-21-22-dl-g1 (Building 4)
        B4-DMZ: 172.17.253.114

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

### Task assigned to each team member

-   T.3.1 - Francisco Oliveira (1201545 - Sprint Master)

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 1.

-   T.3.2 - Miguel Marques (1201529)

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 2.
Final integration of each member’s Packet Tracer simulation into a
single simulation.


-   T.3.3 - Luis Costa (1201459)

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 3.


-   T.3.4 - Guilherme Sousa (1201527)

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 4.