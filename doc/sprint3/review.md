
RCOMP 2021-2022 Project - Sprint 3 review
=========================================
### Sprint master: 1201545 ###

# 1. Sprint's backlog #
In this sprint, we will keep working on the same network simulation from the
previous sprint (regarding the same building). From the already established
layer three configurations, now OSPF based dynamic routing will be used to
replace static routing used previously.


# 2. Subtasks assessment #

###### Totally implemented with issues. ####
-  T.3.1 - Francisco Oliveira (1201545) - Sprint Master

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 1.


###### Totally implemented with issues. ####
-   T.3.2 - Miguel Marques (1201529) 

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 2.
Final integration of each member�s Packet Tracer simulation into a
single simulation.


###### Totally implemented with issues. ####
-   T.3.3 - Luis Costa (1201459)

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 3.


###### Totally implemented with issues. ####
-   T.3.4 - Guilherme Sousa (1201527)

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 4
