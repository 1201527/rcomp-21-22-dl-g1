RCOMP 2021-2022 Project - Sprint 3 - Member 1201527 folder
===========================================

**Project in Packet Tracer**
----------------------------
>
> Following the previous' sprint design (and improving upon it), this is building 4's layout in packet tracer
>
![Projeto](b4foto.png)
>
>
> Comparing with the look of the sprint 2 packet tracer, I only added an HTTP server, but the configurations went a lot beyond.

## 1. OSPF - rotas dinâmicas
----------------------------
- Inicialmente, eliminei as tabelas de rotas estáticas no router do edifício.
- Após isso, utilizei o comando "router ospf PROCESS-ID" e o comando "network NETWORK-ADDRESS NETWORK-WILDCARD area AREA-NUMBER" (com o address, wildcard e id da área do backbone no respetivo lugar).


## 2. HTTP servers
----------------------------
-Na descrição do projeto, no ponto 2, é nos pedido que adicionemos um novo servidor à local DMZ network que assegure os serviços HTTP e que criássemos uma págima simples em HTML que identificasse o edíficio.
-Inicialmente foi adicionado o servidor que, posteriormente, foi connectado ao HC, de forma a que os serviços HTTP fossem assegurados.
-Como último passo, foi configurada uma simples página HTML, que poderá ser accesada pelos Pc's presentes em cada edíficio.

## 3. DHCPv4 service
----------------------------
-Na descrição do projeto é nos pedido que configurássemos o router de forma a que este fornecesse o serviço DHCPv4 para todas as redes locais (dentro do prédio).
-Com o recurso às PL's fornecidas pelos docentes (PL05) os nomes das DHCP pool foram:
-B4-WIFI
-B4-F1
-B4-F0
-B4-DMZ
-Depois de configuradas todas as VLAN's, exceto a configuração do Voip que será feita segundo a opção 150 como nos é pedido no enunciado, todos os PC's passam a estar fornecidos com os serviços DHCP.


## 4 - VoIP service ##
----------------------------
- Aos telefones 7960 colocados no packet tracer no sprint anterior foi estabelecida uma ligação atravéz do router.
- Em cada building já se encontravam 2 telefones
- Para cada switch ligado  a um telefone, foram configuradas as portas ligadas ao mesmo utilizado este comando na conexão ao telefone.
- Posteriormente, no router foram criadas "DHCP pools":
- Depois, foram registados os telemóveis e os seus números. O número máximo de telemoveis foi retirado do sprint passado enquanto que os números de ligação foram escolhidos fazendo "numero de edificio x 1000".
- Com estes passos, já era possível fazer a ligação entre telefones do mesmo edificio, no entanto era ainda necessário estabelecer a conexão com outros edificios.
- Para realizar esta conexão foi feito este código para cada "voice VLAN" dos outros edificios:

-dial-peer voice 426 voip

-destination-pattern 1

-session target ipv4:172.17.248.4


Com isto, ficaram os telefones configurados e funcionais.


##5.DNS##
----------------------------
- Foi criado por mim, o nivel mais alto de dominio (rcomp-21-22-dl-g1), de acordo como o pedido no enunciado.
- o DNS server (ns.rcomp-21-22-dl-g1) e o DNS domain name (rcomp-21-22-dl-g1)
- Adicionai os records e os servidores de todos os buildings manualmente como pedido no enunciado


##6.NAT##
----------------------------
- Fiz os comandos no router do building 4 para as portas, 53(DNS), 443(HTTPS) e 80(HTTP).
- Para alem disso efetuei ainda os comandos -ip nat outside e -ip nat inside


## 7. Static Firewall (ACLs)
----------------------------
- Inicialmente, bloqueámos as falsificações internas, exceto a DMZ.
- Após isso, permiti todos os pedidos ICMP e respostas echo
- O tráfego da DMZ para fora foi permitido. Todo o tráfego em direção à DMZ foi bloqueado exceto o tráfego DNS e HTTP/HTTPS.
- De seguida, bloqueei todo o tráfego direcionado a um router com IPV4 e pertencente ao mesmo.
- Finalmente, permiti o tráfego que passa pelo router.