RCOMP 2021-2022 Project - Sprint 1 - Member 1201529 folder
===========================================


### Procedure ###

Using the scale seen in the plants provided in the project description for sprint 1 its was calculated the length and width of each room (using a 3-simple rule) and then the area of each room was properly calculated (width x length). As seen in the following image using the area the proper number of outlets was also calculated according to the already previously established rules (2 outlets for every 10 square meters).

Then the outlets were set in place following the rules once again (every possible spot in every room should have at least one outlet 3 meters away), at this point a clarification should be made related to the outlet’s positioning. Two possible choices could be made here, either putting the outlets in the walls (whenever possible) or putting the on the floor around the room.

The first of the two would likely mean more cable would need to be used, also each outlet’s “connectivity radius” of 3 meters would not be fully taken advantage of as half of the “circumference” would be outside the actual room, so even though with both strategies it is possible to complete the requirements, to give more possibilities to make each room’s disposition to the client’s liking the latter option was chosen. This doesn’t mean the second option comes without its issues though as it means cables would need to pass through the middle of the room so cover ramp guard would be needed.



### Floor 0 ###

Coming in form the outside is optical fiber that connects to the intermediate cross-connect, the IC then connects to the Horizontal cross connect next to it and the one in the floor above. In this case the IC and the HC share a 19’’ 9U telecommunication enclosure, within it there is a 4 port optical fiber patch panel, a 12 CAT6a patch panel (a patch panel with 12 ports was used because although it uses 1U, the same as a 24 port patch panel, the one with 12 ports was significantly cheaper), a 12 CAT6 patch panel, a 4 port switch and a 12 port switch.

Room’s 2.0.2 outlets are directly connected to the Horizontal Cross-Connection of the floor as they are close enough to room 2.0.1. The wires pass through the underground cable raceway.

The underground cable raceway is then used to connect the HC to the Consolidation Point in room 2.0.3, as this is the largest room in the building it is the only one with a consolidation point for outlets only within itself. This CP is in a telecommunication enclosure of 19’’ and 9U, this enclosure has within it 1 patch panel with 12 CAT6a ports, a patch panel with 24 CAT6 ports and a switch with 24 ports as well. This CP connects with 18 outlets as well as an access point that ensures wi-fi over a 40m diameter which means every room is accounted for.

The other CP that exists in this floor is room 2.0.5, as this is a small room this will serve as the CP for the two room at either side of it. The CP once again has 1 patch panel with 12 CAT6a ports, a patch panel with 24 CAT6 ports and a switch with 24 ports. The wires going to either room beside it go through the wall because although there is  an underground cable raceway going in those rooms the fact that those exits are in the far side of the room mean that per cable between 1 and 2 meters would be wasted by going through the raceway, making a hole in each wall a better solution.
	
	
	
### Floor 1 ###

The HC receives optical fiber from the floor below. The HC is within a 19’’ 9U telecommunication enclosure that holds a 4 port optical fiber patch panel, a 12 CAT6a patch panel, a 12 CAT6 patch panel and a 12 port switch. The HC in room 2.1.1 is directly connected to the outlets in room 2.1.2 and 2.1.3, this connection is made by making holes in the walls as if the fake ceiling was used that would mean an extra 5 meters of wire per outlet this is something that will happen in every wire connected to an outlet (the orange ones in the image), this means in total we will save 170 meter of cable by drilling 8 walls.

The connections from the HC to the CP’s are fairly straight forward, this time the connection is done through the fake ceiling, each CP is within a 19’’ 9U telecommunication enclosure that holds a 12 CAT6a patch panel, a 24 CAT6 patch panel and a 24 port switch. As a note, the reason it is a 24 CAT6 patch panel and a 24 port switch on the CP’s located in 2.1.6 and 2.1.7 even though it only connects to 12 outlets is to leave a little room for maneuverability, if a new outlet is required or for some reason a port breaks then it’s not necessary to buy an entirely new patch panel and/or switch.



### Inventory ###

-	119,45 meters of Copper CAT6A
-	530,6 meters of Copper CAT6
-	107 meters of cable cover ramp guard
-	2 horizontal cross-connects
-	1 intermediate cross-connect
-	2 access points
-	5 consolidation points 
-	7 telecommunication enclosure 19’’ 9U 
-	2 optical fiber 4 port patch panels (1U x 2)
-	7 CAT6a 12 port patch panels (1U x 7)
-	5 CAT6 24 port patch panels (1U x 5)
-	5 24 port switches (1U x 5)
-	2 12 port switches (1U x 2)
-	1 4 port switch
-	100 network outlets

(63U total available space)

(22U used space)
