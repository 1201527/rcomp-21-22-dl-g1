RCOMP 2021-2022 Project - Sprint 1 review
=========================================
### Sprint master: 1201527 ###

# 1. Sprint's backlog #

* Rooms area measurement and a resulting standard number of network outlets.
* Pinpoint outlets positions over the provided floor plans
* Decide cross-connects locations
* Define cable pathways and cable types
* Select cable types
* Structured cabling hardware inventory

# 2. Subtasks assessment #
On this division, each team member will assess the work done on this sprint and evaluate with a number from 1 to 5.


### 1201545 - Structured cable design for building 1 and all the buildings' backbone ###
* Evaluation (1/5) -> 4
* Notes -> I got some doubts doing the campus backbone since the alternatives weren't that explicit, but I think I've done the best I could from what I understood from the requirements.

### 1201529 - Structured cable design for building 2 ###
* Evaluation (1/5) -> 5
* Notes -> Overall the sprint went pretty well. The major dificulty for me was actually getting everything needed in the inventory as it seemed that every day I would figure out a new item would be needed, however I have a great belief in the final product.

### 1201459 - Structured cable design for building 3 ###
* Evaluation (1/5) -> 4
* Notes -> I presented some doubts about the positioning of the CP's and what would be the best way for the connections between the CP's and the outlets.

### 1201527 - Structured cable design for building 4 ###
* Evaluation (1/5) -> 4
* Notes -> I had a little trouble on the cross connects' positioning because I tried to position it to use the less cable possible. I also had some difficulties on finding everything that was supposed to be written on the global inventory. Other than that I think the sprint went well.

