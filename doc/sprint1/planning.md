RCOMP 2021-2022 Project - Sprint 1 planning
===========================================
### Sprint master: 1201527 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
* Rooms area measurement and a resulting standard number of network outlets.
* Pinpoint outlets positions over the provided floor plans
* Decide cross-connects locations
* Define cable pathways and cable types
* Select cable types
* Structured cabling hardware inventory

# 2. Technical decisions and coordination #
* We chose the 586A Copper cable wiring standard.
* To connect each building's IC to the MC on building 1 we selected optical fibre.
* To connect cross connects to the outlets we chose the cable type Copper CAT6.
* To connect cross connects to cross connects, cross connects to consolidation points and access points to consolidation points we chose the cable type Copper CAT6A.
* In the upper floors, we decided that we will be making holes in the wall when passing Copper CAT6 cables to another rooms, rather than passing it using the removable dropped ceiling.
* When passing cables through the ground, we decided to use a cable cover ramp guard to ensure cable's security and also the security of people on this building campus.
* Each one of every building's access point will be on different frequencies (a difference about 22 mHZ).

# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 1)

* T.1.1 assigned to Francisco Oliveira (1201545)
* T.1.2 assigned to Miguel Marques (1201529)
* T.1.3 assigned to Luís Costa (1201459)
* T.1.4 assigned to Guilherme Sousa (1201527) --> Sprint Master