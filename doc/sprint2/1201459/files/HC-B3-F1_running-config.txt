!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
 switchport mode trunk
!
interface Ethernet3/1
 switchport access vlan 425
 switchport mode access
 switchport voice vlan 431
!
interface Ethernet4/1
 switchport access vlan 425
!
interface Ethernet5/1
 switchport access vlan 425
!
interface Ethernet6/1
 switchport mode trunk
!
interface Ethernet7/1
 switchport mode trunk
!
interface Ethernet8/1
 switchport mode trunk
!
interface Ethernet9/1
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

