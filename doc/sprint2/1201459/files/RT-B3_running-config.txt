!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017R6X8-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet1/0
 ip address 172.17.248.3 255.255.255.192
!
interface FastEthernet1/0.422
 encapsulation dot1Q 422
 ip address 172.17.251.65 255.255.255.192
!
interface FastEthernet1/0.425
 encapsulation dot1Q 425
 ip address 172.17.252.1 255.255.255.192
!
interface FastEthernet1/0.427
 encapsulation dot1Q 427
 ip address 172.17.252.129 255.255.255.192
!
interface FastEthernet1/0.428
 encapsulation dot1Q 428
 ip address 172.17.252.193 255.255.255.224
!
interface FastEthernet1/0.431
 encapsulation dot1Q 431
 ip address 172.17.253.33 255.255.255.224
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 172.17.248.128 255.255.255.128 172.17.248.1 
ip route 172.17.249.0 255.255.255.128 172.17.248.2 
ip route 172.17.249.128 255.255.255.128 172.17.248.1 
ip route 172.17.250.0 255.255.255.128 172.17.248.1 
ip route 172.17.250.128 255.255.255.128 172.17.248.4 
ip route 172.17.251.0 255.255.255.192 172.17.248.1 
ip route 172.17.251.128 255.255.255.192 172.17.248.4 
ip route 172.17.251.192 255.255.255.192 172.17.248.2 
ip route 172.17.252.64 255.255.255.192 172.17.248.1 
ip route 172.17.252.224 255.255.255.224 172.17.248.4 
ip route 172.17.253.0 255.255.255.224 172.17.248.2 
ip route 172.17.253.64 255.255.255.240 172.17.248.2 
ip route 172.17.253.80 255.255.255.240 172.17.248.2 
ip route 172.17.253.96 255.255.255.240 172.17.248.4 
ip route 172.17.253.112 255.255.255.240 172.17.248.4 
ip route 0.0.0.0 0.0.0.0 172.17.248.5 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

