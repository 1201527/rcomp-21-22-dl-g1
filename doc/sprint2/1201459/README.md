RCOMP 2021-2022 Project - Sprint 2 - Member 1201459 folder
===========================================
#### Building 3 ####
===========


**Planning**
---------------------------

> When planning the sprint, each building's router got it's own IP, building 3's IP is 172.17.248.3


>
>
> Before starting the project, every VLAN dedicated to the group was properly distributed, building 3's VLANs:
>    * VLAN *422*: B3-WIFI : 55 *nodes*;
>    * VLAN *425*: B3-F1 : 45 *nodes*;
>    * VLAN *427*: B3-F0 : 35 *nodes*;
>    * VLAN *428*: B3-DMZ : 28 *nodes*;
>    * VLAN *431*: B3-VOIP : 25 *nodes*;
>
> When subnetting we gave each VLAN an ip
> This can be seen in the image below
> 
>
![Projeto](building3network.png)
> 
> Even though these are the only VLANs associated with building 3, every VLAN is present in the simulation




>
**Project in Packet Tracer**
----------------------------
>
> Following the previous' sprint design (and improving upon it), this is building 3's layout in packet tracer 
> 
![Projeto](Building3_F1_pkt.png)
>
>
>
![Projeto](Building3_F0_ptk.png)
>
> 
>
>Again, even though not part of the building, the backbone's, including the other building's IC is present
>
>The connection to the internet is represented by a cloud with the ip of 15.203.48.106/30	



>
> All doors not connected to end devices are in trunk mode 
> 

> For each building, a set of requirement were made relating to the end nodes (objects ) 
>   * To the VLAN for the ground-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the first-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the wi-fi it was connected an access point per floor, with a laptop per access point (it was only necessary 1 in total)
>   * To the VLAN for the servers it was connected a server per floor (it was only necessary 1 in total)
>   * To the VLAN for the phones it was connected a phone per floor (it was only necessary 1 in total)
