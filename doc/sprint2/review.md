RCOMP 2021-2022 Project - Sprint 2 review
=========================================
### Sprint master: 1201529 ###

# 1. Sprint's backlog #
In this second sprint the team will create a network simulation matching the developed structured cabling
project of the first sprint. In this sprint the focus is on the layer two infrastructure, and the layer three
fundamentals (IPv4 addressing, and static routing).
For such purposes, the Cisco Packet Tracer is the tool will be used


-  T.2.1 - Francisco Oliveira (1201545)

Development of a layer two and layer three Packet Tracer
simulation for building one, encompassing the campus
backbone.
Integration of every member’s Packet Tracer simulation into
a single simulation.


-   T.2.2 - Miguel Marques (1201529) - Sprint Master

Development of a layer two and layer three Packet Tracer
simulation for building two, encompassing the campus
backbone.


-   T.2.3 - Luis Costa (1201459)

Development of a layer two and layer three Packet Tracer
simulation for building three, encompassing the campus
backbone.


-   T.2.4 - Guilherme Sousa (1201527)

Development of a layer two and layer three Packet Tracer
simulation for building four, encompassing the campus
backbone.

# 2. Subtasks assessment #

One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

* Totally implemented with no issues
* Totally implemented with issues
* Partially implemented with no issues
* Partially implemented with issues




## T.2.1. 1201545 - Structured cable design for building A, floors 2 and 3 #
### Totally implemented with no issues. ###

## T.2.2. 1201529 - Structured cable design for building B, floors 0 and 1 #
### Totally implemented with no issues. ###

## T.2.3. 1201459 - VLAN devices configuration for building C #
### Totally implemented with issues. ###

Everything is totally implemented and running fine, however, unlike the rest of the team that used Fast Ethernet cable when dealing with copper, there were various instances where this member used regular ethernet cables.
This should not affect the overall project, as everything still works as intended, however it is technically slower and different from the other members.

## T.2.4. 1201527 - IPv4 addressing and routing configurations for building D #
### Totally implemented with no issues. ###

