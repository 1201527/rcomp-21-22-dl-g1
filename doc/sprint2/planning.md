RCOMP 2021-2022 Project - Sprint 2 planning
===========================================
### Sprint master: 1201529 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
In this second sprint the team will create a network simulation matching the developed structured cabling
project of the first sprint. In this sprint the focus is on the layer two infrastructure, and the layer three
fundamentals (IPv4 addressing, and static routing).
For such purposes, the Cisco Packet Tracer is the tool will be used

# 2. Technical decisions and coordination #

-   All member used Cisco Packet Tracer version 8.1.1.0022

-   Devices naming convention follows the rule:
    "DeviceName"-"BuildingName"-"Floor" i.e.(IC-B4-F0)
    
-    The Packet Tracer simulation file should be named buildingN.pkt, with N replaced by the digit identifying the building.

-   VTP domain name must be rc22dkg1

-   VLANIDs range must be (415-445), this range must include the backbone and 5 VLANs per building.

-   The ISP router IPv4 node address to be used was 15.203.48.106/30

-   The IPv4 address space to be used was 172.17.248.0/21, which was properly distributed

-   MC's IP is 172.17.248.5; Buildings 1, 2, 3 and 4 have IP's 172.17.248.1 to 172.17.248.4 respectively

-   The previous 5 points can all be seen in the image below:

    ![Planning](planning.png)



# 3. Subtasks assignment #

-  T.2.1 - Francisco Oliveira (1201545)

Development of a layer two and layer three Packet Tracer
simulation for building one, encompassing the campus
backbone.
Integration of every member’s Packet Tracer simulation into
a single simulation.


-   T.2.2 - Miguel Marques (1201529) - Sprint Master

Development of a layer two and layer three Packet Tracer
simulation for building two, encompassing the campus
backbone.


-   T.2.3 - Luis Costa (1201459)

Development of a layer two and layer three Packet Tracer
simulation for building three, encompassing the campus
backbone.


-   T.2.4 - Guilherme Sousa (1201527)

Development of a layer two and layer three Packet Tracer
simulation for building four, encompassing the campus
backbone.

