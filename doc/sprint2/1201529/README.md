RCOMP 2021-2022 Project - Sprint 2 - Member 1201529 folder
===========================================
#### Building 2 ####
===========


**Planning**
---------------------------

> When planning the sprint, each building's router got it's own IP, building 2's IP is 172.17.248.2


>
>
> Before starting the project, every VLAN dedicated to the group was properly distributed, building 2's VLANs:
>    * VLAN *417*: B2-WIFI : 120 *nodes*;
>    * VLAN *424*: B2-F1 : 50 *nodes*;
>    * VLAN *430*: B2-F0 : 25 *nodes*;
>    * VLAN *432*: B2-DMZ : 12 *nodes*;
>    * VLAN *433*: B2-VOIP : 12 *nodes*;
>
> When subnetting we gave each VLAN an ip
> This can be seen in the image below
> 
>
![Projeto](building2network.png)
> 
> Even though these are the only VLANs associated with building 2, every VLAN is present in the simulation




>
**Project in Packet Tracer**
----------------------------
>
> Following the previous' sprint design (and improving upon it), this is building 2's layout in packet tracer 
> 
![Projeto](building2_pkt.png)
> 
>
>Again, even though not part of the building, the backbone's, including the other building's IC is present
>
>The connection to the internet is represented by a cloud with the ip of 15.203.48.106/30


>
> All doors not connected to end devices are in trunk mode 
> 

> For each building, a set of requirement were made relating to the end nodes (objects ) 
>   * To the VLAN for the ground-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the first-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the wi-fi it was connected an access point per floor, with a laptop per access point (it was only necessary 1 in total)
>   * To the VLAN for the servers it was connected a server per floor (it was only necessary 1 in total)
>   * To the VLAN for the phones it was connected a phone per floor (it was only necessary 1 in total)
