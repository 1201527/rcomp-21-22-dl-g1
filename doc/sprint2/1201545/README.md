RCOMP 2021-2022 Project - Sprint 2 - Member 1201545 folder
===========================================
#### Building 1 ####
===========


**Planning**
---------------------------

> When planning the sprint, each building's router got it's own IP, building 2's IP is 172.17.248.1


>
>
> Before starting the project, every VLAN dedicated to the group was properly distributed, building 2's VLANs:
>    * VLAN *416*: B1-WIFI : 120 *nodes*;
>    * VLAN *419*: B1-F1 : 80 *nodes*;
>    * VLAN *421*: B1-F0 : 60 *nodes*;
>    * VLAN *418*: B1-DMZ : 100 *nodes*;
>    * VLAN *426*: B1-VOIP : 40 *nodes*;
>    * VLAN *415*: B1-BACKBONE : 120 *nodes*;
>
> When subnetting we gave each VLAN an ip
> This can be seen in the image below
> 
>
![Projeto](vlan.png)
> 
> Even though these are the only VLANs associated with building 1 and the backbone, every VLAN is present in the simulation

>
**Project in Packet Tracer**
----------------------------
>
> Following the previous' sprint design (and improving upon it), this is building 1's layout in packet tracer and the campus's layout
> 
![Projeto](building1.png)
![Projeto](campus.png)
> 
> 
>The connection to the internet is represented by a cloud with the ip of 15.203.48.106/30


>
> All doors not connected to end devices are in trunk mode 
> 

> For each building, a set of requirement were made relating to the end nodes (objects ) 
>   * To the VLAN for the ground-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the first-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the wi-fi it was connected an access point per floor, with a laptop per access point (it was only necessary 1 in total)
>   * To the VLAN for the servers it was connected a server per floor (it was only necessary 1 in total)
>   * To the VLAN for the phones it was connected a phone per floor (it was only necessary 1 in total)
