RCOMP 2021-2022 Project - Sprint 2 - Member 1201527 folder
===========================================
#### Building 4 ####
===========


**Planning**
---------------------------

> When planning the sprint, each building's router got it's own IP, building 4's IP is 172.17.248.4

>
>
> Before starting the project, every VLAN dedicated to the group was properly distributed, building 4's VLANs:
>    * VLAN *420*: B4-WIFI : 70 *nodes*;
>    * VLAN *423*: B4-F1 : 55 *nodes*;
>    * VLAN *429*: B4-F0 : 28 *nodes*;
>    * VLAN *434*: B4-VOIP : 12 *nodes*;
>    * VLAN *435*: B4-DMZ : 10 *nodes*;
>
> When subnetting we gave each VLAN an ip
> This can be seen in the image below
> 
>
![Projeto](excelBuild4.PNG)
> 
> Even though these are the only VLANs associated with building 4, every VLAN is present in the simulation


>
**Project in Packet Tracer**
----------------------------
>
> Following the previous' sprint design (and improving upon it), this is building 4's layout in packet tracer 
> 
![Projeto](printBuilding4.png)
> 
>
>Again, even though not part of the building, the backbone's, including the other building's IC is present
>
>The connection to the internet is represented by a cloud with the ip of 15.203.48.106/30


>
> All doors not connected to end devices are in trunk mode 
> 

> For each building, a set of requirement were made relating to the end nodes (objects ) 
>   * To the VLAN for the ground-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the first-floor it was connected a PC per HC/CP (it was only necessary 1 in total)
>   * To the VLAN for the wi-fi it was connected an access point per floor, with a laptop per access point (it was only necessary 1 in total)
>   * To the VLAN for the servers it was connected a server per floor (it was only necessary 1 in total)
>   * To the VLAN for the phones it was connected a phone per floor (it was only necessary 1 in total)
